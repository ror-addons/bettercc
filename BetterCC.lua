--[[--------------------------------------------------------------------------------------------------------------
Copyright (c) 2010 Brad Bates

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
--]]--------------------------------------------------------------------------------------------------------------
BetterCC = {}

local newAlpha  = 1 
local uiscale = InterfaceCore.GetScale()
local cooldowns = {}
local activePulses = {}

local DAY, HOUR, MINUTE, SHORT = 86400, 3600, 60, 5 --values for time
local floor = math.floor
local min, max = math.min, math.max
local BASE_ICON         = 0
local BUTTON_TEXT       = 1
local COOLDOWN          = 2
local COOLDOWN_TIMER    = 3
local FLASH_ANIM        = 4
local ACTIVE_ANIM       = 5
local GLOW_ANIM         = 6
local STACK_COUNT_TEXT  = 7

local UpdateCooldownAnimation

function BetterCC.Initialize()
	UpdateCooldownAnimation = ActionButton.UpdateCooldownAnimation or false
	ActionButton.UpdateCooldownAnimation = BetterCC.BetterCCAnim
end

function BetterCC.BetterCCAnim(self, timeElapsed, updateCooldown, ...)
	if UpdateCooldownAnimation then
		UpdateCooldownAnimation(self, timeElapsed, updateCooldown, ...)
    end
		
	local labelTime
	if(self.m_Cooldown >= HOUR)then
		labelTime = string.format("%dh", floor(self.m_Cooldown/HOUR))
	elseif(self.m_Cooldown >= MINUTE)then	
		labelTime = string.format("%d:%02d", floor(self.m_Cooldown/60), self.m_Cooldown % MINUTE)
	else
		labelTime = string.format("%d", floor(self.m_Cooldown))
	end
					
	self.m_Windows[COOLDOWN_TIMER]:SetText(labelTime)
	self.m_Windows[COOLDOWN_TIMER]:SetFont("font_clear_large_bold", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	self.m_Windows[COOLDOWN_TIMER]:SetTextColor(255, 255, 0)
	if(self.m_Cooldown < 10)then
		self.m_Windows[COOLDOWN_TIMER]:SetTextColor(255, 0, 0)
	end
		
	if(self.m_Cooldown > 1 and self.m_MaxCooldown > ActionButton.GLOBAL_COOLDOWN) then
		self.m_Windows[COOLDOWN_TIMER]:Show(true)
		self.m_Windows[COOLDOWN]:SetAlpha(newAlpha)			
		self.m_Windows[COOLDOWN]:Show(true)
		if(not cooldowns[self.m_Name]) then
			BetterCC.AddToList(self)
		end
	else
		self.m_Windows[COOLDOWN_TIMER]:Show(false)
	end
end

function BetterCC.AddToList(frame)
	cooldowns[frame.m_Name] = frame.m_Cooldown
end

function BetterCC.UpdateAll(timePassed)
	if(next(cooldowns)) then
		for k,v in pairs(cooldowns) do
			BetterCC.UpdateCooldown(k, timePassed)
		end
	end
end

function BetterCC.UpdateCooldown(window, timePassed)
	cooldowns[window] = cooldowns[window] - timePassed
	
	if(cooldowns[window] <= 0) then
		cooldowns[window] = nil
		BetterCC.CreatePulse(window)
	end
end

function BetterCC.CreatePulse(window)
	WindowRegisterCoreEventHandler(window, "OnUpdate", "BetterCC.UpdatePulse")
	activePulses[window] = {}
end

function BetterCC.UpdatePulse(timePassed)
	local self = SystemData.ActiveWindow.name
	local windowName = self.."ActionIcon"
	local windowScale = WindowGetScale(self)
	
	local pulse = activePulses[self]
	
	if not pulse.scale then
		pulse.scale = 1
	end
	
	if pulse.scale >= 2 then
		pulse.dec = 1
	end

	pulse.scale = max(min(pulse.scale + (pulse.dec and -1 or 1) * pulse.scale * (timePassed/0.5), 2), 1)
	
	if pulse.scale <= 1 then
		pulse.dec = nil
		WindowSetAlpha(windowName, 1)
		WindowClearAnchors(windowName)
		WindowSetParent(windowName, self)
		WindowAddAnchor(windowName, "center", self, "center", 0, 0)
		WindowSetLayer(windowName, 0)
		WindowSetScale(windowName, windowScale)
		WindowUnregisterCoreEventHandler(self, "OnUpdate")
		activePulses[self] = nil
	else
		WindowSetAlpha(windowName, 0.5)
		WindowClearAnchors(windowName)
		WindowSetParent(windowName, "Root")
		WindowAddAnchor(windowName, "center", self, "center", 0, 0)
		WindowSetLayer(windowName, 4)
		WindowSetScale(windowName, pulse.scale * windowScale)
	end
end

function BetterCC.UnHook()
	ActionButton.UpdateCooldownAnimation = UpdateCooldownAnimation
end